# LaTeX Samples

Dieses Paket beinhaltet diverse Beispiele für LaTeX.
Die PDFs wurden mit **XeLaTeX** erstellt

## Installation

### LaTeX Basispaket

- MikTex (Win, Linux, Mac): https://miktex.org/
- TexLive (Win, Linux, Mac): https://tug.org/texlive/

Persönliche Nutzung/Erfahrung: 

- Windows, MacOS: Miktex herunterladen und installieren
- Linux: `apt-get install texlive texlive-lang-german texlive-latex-extra`

Nicht vorhandene Packages wie `xcolor`, die innerhalb der `.tex`-Dokumente genutzt werden, lädt die Laufzeitumgebung 
automatisch herunter und installiert diese. Sobald das Package installiert wurde, geht die Erstellung vom PDF schneller.
Daher nicht wundern, wenn das PDF beim ersten Mal etwas länger braucht.

### IDEs / Editoren

Die nachfolgenden Editoren sind eine kleine Auswahl von LaTeX-Editoren. Für die Preäsentation
habe ich *TexStudio* genutzt. Overleaf ist ein LaTeX-Editor im Browser.
Es ist Open-Source (https://github.com/overleaf/overleaf) und kann auch selbst gehostet werden.

- TexStudio - https://www.texstudio.org/
- TexMaker - https://www.xm1math.net/texmaker/
- TexWorks - https://www.tug.org/texworks/
- TeXnicCenter - https://www.texniccenter.org/
- LyX (WYSIWYG) - https://www.lyx.org/Download
- Overleaf (Online) - https://www.overleaf.com/
- VS Code Plugin - https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop
  
## Hinweise

- Die Dateien wurden mit XeTeX erstellt. Es kann sein, dass sich die Pakete/Dateien bei anderen Distributionen anders verhalten oder aussehen
- XeTeX unterstützt UTF-8
- Eventuell muss der Standardkompiler in den Einstellungen geändert werden (Texstudio: Options > Configure TexStudio > Build > Default Compiler)
- Die IDEs wie TexStudio erlauben mit `Strg + Linke Maustaste` den Bereich im Dokument oder PDF hervorzuheben oder in andere Dateien zu springen

## Quellen/Dokumentation

- CTAN (https://www.ctan.org/): Hier finden sich generell die Handbücher zu den Packages
- Koma-script (https://www.ctan.org/pkg/koma-script): Das Handbuch zum KOMA-Package (scrartcl, scrlttr2)

