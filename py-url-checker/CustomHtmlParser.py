from html.parser import HTMLParser


class CustomHtmlParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.urls = []

    def handle_starttag(self, tag, attrs):
        if tag != "a":
            return

        for name, value in attrs:
            # If href is defined, print it.
            if name == "href":
                self.urls.append(value)
