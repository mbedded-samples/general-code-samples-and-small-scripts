"""
    This script loads all posts of your Ghost instance
    and extracts the urls (html a-tag).
    Afterwards every Url is checked (http get) to verify
    if the site can be reached.
    If the request was successful the url is considered as "works correctly".
"""

import json
import time
import requests
from joblib import Parallel, delayed

from CustomHtmlParser import CustomHtmlParser

# Base Url of Ghost API
BASE_URL = 'http://localhost:3001'
# This token must be created within your ghost instance
URL_KEY = 'PUT_YOUR_TOKEN_HERE'
URL_LIMIT = 20
SUMMARY_FILE = 'summary.txt'


def extract_urls_from_posts():
    total_pages = 1
    current_page = 1
    all_urls = {}

    while current_page <= total_pages:
        url = create_url(current_page)
        response = requests.get(url)

        data = json.loads(response.text)

        for post in data['posts']:
            found_urls = extract_urls(post)

            for url in found_urls:
                if not url.startswith("http"):
                    continue

                affected_posts = all_urls.get(url)

                if affected_posts is None:
                    all_urls[url] = [post]
                else:
                    affected_posts.append(post)

        total_pages = data['meta']['pagination']['pages']
        current_page += 1

    return all_urls


def create_url(page):
    return f'{BASE_URL}/ghost/api/v4/content/posts?key={URL_KEY}&fields=id,title,url,html&limit={URL_LIMIT}&page={page}'


def extract_urls(post):
    text = post['html']

    parser = CustomHtmlParser()
    parser.feed(text)

    return parser.urls


def check_all_urls(urls):
    good_urls = []
    bad_urls = []
    exception_urls = []

    Parallel(n_jobs=4, require='sharedmem')(delayed(lambda param:
                                                    checkUrl(param, good_urls, bad_urls, exception_urls)
                                                    )
                                            (url) for url in urls.keys())

    return good_urls, bad_urls, exception_urls


def checkUrl(url, good_urls, bad_urls, exception_urls):
    try:
        result = requests.get(url, timeout=10)
    except Exception as err:
        item = (url, str(err), None)
        exception_urls.append(item)
        return

        # status code (int), duration in milliseconds
    item = (url, result.status_code, result.elapsed.microseconds / 1000)

    if result.status_code == 200:
        good_urls.append(item)
    else:
        bad_urls.append(item)


def export_result(urls, good_urls, bad_urls, exception_urls, duration):
    with open(SUMMARY_FILE, 'w', encoding='UTF-8') as file:
        write_urls_to_file(f'# Exceptions  ({len(exception_urls)})', exception_urls, file, urls, True)
        write_urls_to_file(f'\n\n# Bad ({len(bad_urls)})', bad_urls, file, urls)
        write_urls_to_file(f'\n\n# Good ({len(good_urls)})', good_urls, file, urls)

        file.writelines([f'\n# SUMMARY\n',
                         f'Total urls: {len(urls):2d}\n',
                         f'Duration:   {duration:05.2f} sec\n'])


def write_urls_to_file(title, url_list, file, urls, is_exception=False):
    file.write(f'{title}\n')
    if len(url_list) == 0:
        file.write(" - This list is empty\n")
    else:
        write_url_to_file(file, url_list, urls, is_exception)


def write_url_to_file(file, url_list, all_urls, is_exception):
    for (url, status, duration) in url_list:
        posts = all_urls.get(url)

        if is_exception:
            file.write(f'{url}, Message: {status}\n')
        else:
            file.write(f'{url}, Status: {status}, Duration: {duration} ms\n')

        for post in posts:
            file.write(f'-  {post["url"]} ({post["title"]})\n')


if __name__ == '__main__':
    start = time.time()

    urls = extract_urls_from_posts()
    good_urls, bad_urls, exception_urls = check_all_urls(urls)

    end = time.time()
    duration = end - start

    export_result(urls, good_urls, bad_urls, exception_urls, duration)
