"""
    Converts json data to html and pushes via ghost api
"""
import json
import sys
from datetime import datetime as date
import jwt
import requests

# Doesn't matter if trailing slash or not. Ghost seems to be robust top handle both
BASE_URL = 'http://localhost:3001'
DATA_FILE = 'tools.json'

PAGE_ID = ''
URL_KEY = ''
URL_ADMIN_KEY = ''

TMP_CONTENT = "@HTML_CONTENT@"
PLACEHOLDER_NAME = "@PLACEHOLDER_NAME@"
PLACEHOLDER_WEBSITE_URL = "@PLACEHOLDER_WEBSITE_URL@"
PLACEHOLDER_DESCRIPTION = "@PLACEHOLDER_DESCRIPTION@"

TEMPLATE_MOBILEDOC = f'''
                    {{
                      "version": "0.3.1",
                      "atoms": [],
                      "cards": [
                        [
                          "html",
                          {{
                            "html": "{TMP_CONTENT}"
                          }}
                        ]
                      ],
                      "markups": [],
                      "sections": [
                        [
                          10,
                          0
                        ]
                      ],
                      "ghostVersion": "4.0"
                    }}
                    '''

TEMPLATE_HTML_CONTAINER = f"""
<div class='tool-container'>
    <h2 class='tool-title'>{PLACEHOLDER_NAME}</h2>
    <div class='tool-website'>
        <span>
            Webseite: <a href='{PLACEHOLDER_WEBSITE_URL}'>{PLACEHOLDER_WEBSITE_URL}</a>
        </span>
    </div>
    <p class='tool-description'>{PLACEHOLDER_DESCRIPTION}</p>
</div>
"""


def generate_token():
    print("Generate token")
    # Split the key into ID and SECRET
    id, secret = URL_ADMIN_KEY.split(':')

    # Prepare header and payload
    iat = int(date.now().timestamp())

    header = {'alg': 'HS256', 'typ': 'JWT', 'kid': id}
    payload = {
        'iat': iat,
        'exp': iat + 5 * 60,
        'aud': '/v3/admin/'
    }

    # Create the token (including decoding secret)
    return jwt.encode(payload, bytes.fromhex(secret), algorithm='HS256', headers=header)


def page_get_url():
    return f'{BASE_URL}/ghost/api/v4/content/pages/{PAGE_ID}?key={URL_KEY}'


def page_put_url():
    return f'{BASE_URL}/ghost/api/v3/admin/pages/{PAGE_ID}'


def get_update_date():
    url = page_get_url()

    print(f"Try GET: {url}")
    response = requests.get(url)

    if response.status_code == 200:
        print("GET request successful")
    else:
        raise Exception(f"Error receiving content. response.text: {response.text}")

    data = json.loads(response.text)
    return data['pages'][0]['updated_at']


def parse_tool_data_from_json():
    content = []

    with open(DATA_FILE, 'r', encoding='utf-8') as file:
        data_json = json.load(file)

    content.append(f"<p>{data_json['intro-text']}</p>")
    content.append(f"<p class='text-muted'>Updated at: {date.now().strftime('%Y-%m-%d')}</p>")

    for tool in sorted(data_json['tools'], key=lambda x: x['name']):
        content.append(tool_to_html(tool))

    return data_json['title'], "".join(content)


def tool_to_html(tool):
    return TEMPLATE_HTML_CONTAINER.replace(PLACEHOLDER_NAME, tool['name']) \
        .replace(PLACEHOLDER_WEBSITE_URL, tool['website']) \
        .replace(PLACEHOLDER_DESCRIPTION, tool['description'])


def generate_update_body(title, html_content, update_at):
    page = dict()
    page['title'] = title

    # Replace of linebreak needed because API will throw "invalid JSON" error
    html_content = html_content.replace("\n", "")
    page['mobiledoc'] = TEMPLATE_MOBILEDOC.replace(TMP_CONTENT, html_content)
    page['updated_at'] = update_at

    body = dict()
    body['pages'] = [page]

    return body


def update_page(body):
    url = page_put_url()
    header = {
        'Content-Type': 'application/json',
        'Authorization': 'Ghost ' + token_str
    }
    data = json.dumps(body)

    print(f"Try: PUT {url}")
    response = requests.put(url, headers=header, data=data)

    if response.status_code == 200:
        print("PUT request successful")
    else:
        raise Exception(f"Error update content. response.text: {response.text}")


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print("Invalid number of arguments. 3 arguments are needed")
        print("Usage: python script.py [PAGE_ID] [URL_KEY] [URL_ADMIN_KEY]")
        sys.exit("Invalid number of arguments")

    PAGE_ID = sys.argv[1]
    URL_KEY = sys.argv[2]
    URL_ADMIN_KEY = sys.argv[3]

    token = generate_token()
    token_str = str(token).replace('b\'', '').replace('\'', '')

    update_at = get_update_date()
    print(f"Received post update date: {update_at}")

    title, htmlContent = parse_tool_data_from_json()
    body = generate_update_body(title, htmlContent, update_at)

    update_page(body)

    print("Page updated")
