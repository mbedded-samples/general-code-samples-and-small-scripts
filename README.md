# Samples

This repository contains general code or project samples to demonstrate a feature, proof-of-concept or just useful scripts. Based on the complexity they may contain an additional readme file.
In the worst case you find a general information in this file about the project.
See the list of projects below for details.

## Prefixes

The folders have a prefix so you can identiy the programming
language on a first glance. The following prefixes are in use:

- `dotnet` = .NET (usually C#)
- `py` = Python
- `tex` = LaTeX/XeLaTeX

## Project overview

The projects are sorted by their name without the prefix.

| Project               | Language | Description                                                                                                                                                                              |
| :-------------------- | :------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| chat-prototype        | C#       | A sample console chat application using C#.                                                                                                                                              |
| multiple-targets      | C#       | A sample project written in C# to create a library which supports multiple targets like .NET Framework and .NET Core.                                                                    |
| ghost-page-update     | Python   | This sample script reads an defined json file to create a HTML-List and push this content to a [Ghost](https://ghost.org/) based website.                                                |
| sample-real-time-rest | C#       | A sample in C# to demonstrate long-polling and a "real time" communication using events.                                                                                                 |
| url-checker           | Python   | This is a simple python script which extracts the urls based on [Ghost](https://ghost.org/) and does a http-request to verify if the url can be reached. The result is exported as file. |
| full-presentation     | LaTeX    | A full presentation and example about various features in LaTeX.                                                                                                                        |


